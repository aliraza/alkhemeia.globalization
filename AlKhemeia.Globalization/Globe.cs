﻿using AlKhemeia.Geo.Interfaces;
using AlKhemeia.Geo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlKhemeia.Geo
{
   public class Globe
   {
      private static ICountryRepository _countries;
      private static ICityRepository _cities;

      /// <summary>
      /// 
      /// </summary>
      public static ICountryRepository Countries
      {
         get
         {
            if (_countries == null)
            {
               _countries = new CountryRepository();
            }
            return _countries;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      public static ICityRepository Cities
      {
         get
         {
            if (_cities == null)
            {
               _cities = new CityRepository();
            }
            return _cities;
         }
      }
   }
}
