﻿using AlKhemeia.Geo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AlKhemeia.Utilities.ExtensionMethods;
using System.IO;

namespace AlKhemeia.Geo.Repository
{
   internal class BaseRepository<TModel> : IBaseRepository<TModel>
   {
      #region Members
      private XElement _dbContext;
      private List<TModel> _allElements;
      #endregion

      #region Properties
      protected IList<TModel> AllElements
      {
         get
         {
            if (_allElements == null)
            {
               _allElements = new List<TModel>();
               var listElements = Context.Elements();
               foreach (var element in listElements)
               {
                  _allElements.Add(element.FromXElement<TModel>());
               }

            }
            return _allElements;
         }
      }

      /// <summary>
      /// 
      /// </summary>
      protected string XmlSource { get; set; }

      /// <summary>
      /// 
      /// </summary>
      public XElement Context
      {
         get
         {
            if (_dbContext == null)
            {
               byte[] byteArray = Encoding.UTF8.GetBytes(XmlSource);
               using (MemoryStream stream = new MemoryStream(byteArray))
               {
                  _dbContext = XElement.Load(stream);
               }
            }
            return _dbContext;
         }
      }
      #endregion

      #region public methods

      /// <summary>
      /// Finds all entities.
      /// </summary>
      /// <returns></returns>
      public IList<TModel> FindAll()
      {
         return AllElements;
      }

      /// <summary>
      /// Counts this instance.
      /// </summary>
      /// <returns></returns>
      public int Count()
      {
         return AllElements != null ? AllElements.Count() : 0;
      }

      #endregion
   }
}
