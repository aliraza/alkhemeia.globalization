﻿using AlKhemeia.Geo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlKhemeia.Geo.Repository
{
   internal class CityRepository : BaseRepository<City>, ICityRepository
   {
      #region Constructors
      public CityRepository()
      {
         this.XmlSource = Resources.Cities;
      }
      #endregion

      #region Methods
      /// <summary>
      /// 
      /// </summary>
      /// <param name="population"></param>
      /// <returns></returns>
      public List<City> FindCityWithPopulationGreaterThan(int population)
      {
         return AllElements.Where(c => c.Population > population).ToList();
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      public City FindBy(int id)
      {
         return AllElements.Where(m => m.GeoNameId.Equals(id)).FirstOrDefault();
      }

      /// <summary>
      /// Find all cities for the specified country
      /// </summary>
      /// <param name="isoCode">Country ISO code</param>
      /// <returns>List of cities for the specified country</returns>
      public IList<City> FindCitiesByCountry(string isoCode)
      {
         return AllElements.Where(c => c.CountryCode.ToLower().Equals(isoCode.ToLower())).ToList();
      }

      /// <summary>
      /// Find cities which name start with name
      /// </summary>
      /// <param name="name">Name/search string of the city</param>
      /// <returns>List of cities whose name matches the name</returns>
      public IList<City> FindCityByName(string name)
      {
         return AllElements.Where(c => c.Name.ToLower().StartsWith(name.ToLower())).ToList();
      }

      /// <summary>
      /// Calculates the distance between two countries
      /// </summary>
      /// <param name="isoCountryCode1">ISO country code1</param>
      /// <param name="isoCountryCode2">ISO country code2</param>
      /// <returns>Distance in Kilometers</returns>
      public double CalculateDistance(string city1, string city2)
      {
         City capital1 = FindCityByName(city1).FirstOrDefault();
         City capital2 = FindCityByName(city2).FirstOrDefault();
         AlKhemeia.Utilities.Position posCountry1 = new AlKhemeia.Utilities.Position { Latitude = capital1.Latitude, Longitude = capital1.Longitude };
         AlKhemeia.Utilities.Position posCountry2 = new AlKhemeia.Utilities.Position { Latitude = capital2.Latitude, Longitude = capital2.Longitude };
         var distance = AlKhemeia.Utilities.MathUtility.CalculateDistance(posCountry1, posCountry2);

         return Math.Round(distance, 2);
      }
      #endregion
   }
}
