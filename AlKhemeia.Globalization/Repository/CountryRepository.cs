﻿using AlKhemeia.Geo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlKhemeia.Geo;
using System.Globalization;

namespace AlKhemeia.Geo.Repository
{
    internal class CountryRepository : BaseRepository<Country>, ICountryRepository
    {
        #region Constructors
        public CountryRepository()
        {
            this.XmlSource = Resources.Countries;
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iso"></param>
        /// <returns></returns>
        public Country FindCountry(string iso)
        {
            return AllElements.Where(c => c.ISO.ToLower().Equals(iso.ToLower())).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Country FindBy(string id)
        {
            return FindCountry(id);
        }

        /// <summary>
        /// Finds a country by name
        /// </summary>
        /// <param name="name">country name</param>
        /// <returns>Country</returns>
        public Country FindByName(string name)
        {
            return AllElements.Where(c => c.CountryName.ToLower().Equals(name.ToLower())).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public IList<Country> FindNeighbouringCountries(Country country)
        {
            if(country == null)
            throw new ArgumentNullException();
            List<Country> countries = new List<Country>();
            List<string> countryCodes = !string.IsNullOrEmpty(country.neighbours) ? country.neighbours.Trim(new char[] { '"' }).Split(new char[] { ',' }).ToList() : new List<string>();

            foreach (string countryCode in countryCodes)
            {
            countries.Add(FindCountry(countryCode));
            }
            return countries;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="population"></param>
        /// <returns></returns>
        public IList<Country> FindCountryWithPopulationGreaterThan(int population)
        {
            return AllElements.Where(c => c.Population.Value > population).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="areaSqKm"></param>
        /// <returns></returns>
        public IList<Country> FindCountryWithAreaGreaterThan(int areaSqKm)
        {
            return AllElements.Where(c => c.AreaSqKm.Value > areaSqKm).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="continent"></param>
        /// <returns></returns>
        public IList<Country> FindCountryByContinent(string continent)
        {
            return AllElements.Where(c => c.Continent.ToLower().Equals(continent.ToLower())).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="continent"></param>
        /// <returns></returns>
        public IList<Country> FindCountriesByContinent(string continent)
        {
            List<Country> countries = new List<Country>();
            countries = AllElements.Where(c => c.Continent.Equals(continent)).ToList();

            return countries;
        }

        /// <summary>
        /// Find countries with names containing the specified strings
        /// </summary>
        /// <param name="term">a string</param>
        /// <returns>A list of countries</returns>
        public IList<Country> FindCountriesWithNameContaining(string term)
        {
            return AllElements.Where(c => c.CountryName.ToLower().Contains(term.ToLower())).ToList();
        }

        /// <summary>
        /// Returs continent iso names with its real name counterpart as a keyvalue pair. e.g. af = Africa and as = Asia
        /// </summary>
        /// <returns>List of keyvalue pairs of continents</returns>
        public IList<KeyValuePair<string, string>> FindContinentKeyValuePair()
        {
            List<KeyValuePair<string, string>> models = new List<KeyValuePair<string, string>>();

            var groups = AllElements.Select(c => c.Continent).GroupBy(k => k);
            foreach (var group in groups)
            {
            var continent = string.Empty;
            switch (group.Key.ToLower())
            {
                case "af":
                    continent = "Africa";
                    break;
                case "an":
                    continent = "Antarctica";
                    break;
                case "as":
                    continent = "Asia";
                    break;
                case "eu":
                    continent = "Europe";
                    break;
                case "na":
                    continent = "North America";
                    break;
                case "oc":
                    continent = "Australia";
                    break;
                case "sa":
                    continent = "South America";
                    break;
            }
            models.Add(new KeyValuePair<string, string>(group.Key, continent));
            }

            return models;
        }

        /// <summary>
        /// Calculate the distance between two countries based on their capitals as origins
        /// </summary>
        /// <param name="isoCountryCode1">ISO Country Code</param>
        /// <param name="isoCountryCode2">ISO Country Code</param>
        /// <returns>distance in KM</returns>
        public double CalculateDistance(string isoCountryCode1, string isoCountryCode2)
        {
            ICityRepository cityRepo = new CityRepository();
            Country country1 = FindCountry(isoCountryCode1);
            Country country2 = FindCountry(isoCountryCode2);
            return Globe.Cities.CalculateDistance(country1.Capital, country2.Capital);
        }
        #endregion

    }
}
