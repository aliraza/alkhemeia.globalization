﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlKhemeia.Geo.Interfaces
{
   public interface IBaseRepository<TModel>
   {
      /// <summary>
      /// Finds all entities.
      /// </summary>
      /// <param name="predicate">The predicate.</param>
      /// <returns></returns>
      IList<TModel> FindAll();

      /// <summary>
      /// Counts this instance.
      /// </summary>
      /// <returns></returns>
      int Count();
   }
}
