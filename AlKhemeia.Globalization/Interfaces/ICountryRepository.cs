﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace AlKhemeia.Geo.Interfaces
{
    public interface ICountryRepository : IBaseRepository<Country>
    {
        /// <summary>
        /// Find country by primary key ISO
        /// </summary>
        /// <param name="iso">the primary key</param>
        /// <returns>Country</returns>
        Country FindCountry(string iso);

        /// <summary>
        /// Finds a country by name
        /// </summary>
        /// <param name="name">country name</param>
        /// <returns>Country</returns>
        Country FindByName(string name);

        /// <summary>
        /// Find neighbouring countries of speicified country
        /// </summary>
        /// <param name="country">The country</param>
        /// <returns>List of neighbouring countries</returns>
        IList<Country> FindNeighbouringCountries(Country country);

        /// <summary>
        /// Find countries with the population greater than the specified number
        /// </summary>
        /// <param name="population">The population</param>
        /// <returns>List of countries</returns>
        IList<Country> FindCountryWithPopulationGreaterThan(int population);

        /// <summary>
        /// Find countries with the area greater than specified number
        /// </summary>
        /// <param name="areaSqKm">Area in square kilometers</param>
        /// <returns>List of countries</returns>
        IList<Country> FindCountryWithAreaGreaterThan(int areaSqKm);

        /// <summary>
        /// Find counties by continent
        /// </summary>
        /// <param name="continent">The continent</param>
        /// <returns>List of countries</returns>
        IList<Country> FindCountriesByContinent(string continent);

        /// <summary>
        /// Find countries with names containing the specified strings
        /// </summary>
        /// <param name="term">a string</param>
        /// <returns>A list of countries</returns>
        IList<Country> FindCountriesWithNameContaining(string term);

        /// <summary>
        /// Find continent keyvalue pairs
        /// </summary>
        /// <returns>List of keyvalue pairs of continents</returns>
        IList<KeyValuePair<string, string>> FindContinentKeyValuePair();

        /// <summary>
        /// Calculate the distance between two countries based on their capitals as origins
        /// </summary>
        /// <param name="isoCountryCode1">ISO Country Code</param>
        /// <param name="isoCountryCode2">ISO Country Code</param>
        /// <returns>distance in KM</returns>
        double CalculateDistance(string isoCountryCode1, string isoCountryCode2);
    }

}
