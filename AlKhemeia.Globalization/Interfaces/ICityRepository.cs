﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlKhemeia.Geo.Interfaces
{
   public interface ICityRepository : IBaseRepository<City>
   {
      /// <summary>
      /// Find cities with the population greater than the specified number
      /// </summary>
      /// <param name="population">The population</param>
      /// <returns>List of cities</returns>
      List<City> FindCityWithPopulationGreaterThan(int population);

      /// <summary>
      /// Find all cities for the specified country
      /// </summary>
      /// <param name="isoCode">Country ISO code</param>
      /// <returns>List of cities for the specified country</returns>
      IList<City> FindCitiesByCountry(string isoCode);

      /// <summary>
      /// Find cities which name start with name
      /// </summary>
      /// <param name="name">Name/search string of the city</param>
      /// <returns>List of cities whose name matches the name</returns>
      IList<City> FindCityByName(string name);

      /// <summary>
      /// Calculates approximate air distance between two cities in kilometers
      /// </summary>
      /// <param name="isoCountryCode1">ISO country code1</param>
      /// <param name="isoCountryCode2">ISO country code2</param>
      /// <returns>Distance in Kilometers</returns>
      double CalculateDistance(string city1, string city2);
   }
}
