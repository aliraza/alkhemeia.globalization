﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlKhemeia.Geo
{
    public class Country
    {
        public string ISO { get; set; }
        public string ISO3 { get; set; }
        public string fips { get; set; }
        public string ISONumeric { get; set; }
        public string CountryName { get; set; }
        public string Capital { get; set; }
        public double? AreaSqKm { get; set; }
        public int? Population { get; set; }
        public string Continent { get; set; }
        public string tld { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string Phone { get; set; }
        public string Languages { get; set; }
        public int geonameid { get; set; }
        public string neighbours { get; set; }

        /// <summary>
        /// Get an instance of System.Globalization.CultureInfo object based on country name. 
        /// Issue:
        ///     There is a possibility that a country may have more than one cultures. This approach currently does not address that.
        /// </summary>
        public CultureInfo CultureInfo
        {
            get
            {
                return CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.EnglishName.ToLower().Contains(CountryName.ToLower())).FirstOrDefault();
            }
        }

        /// <summary>
        /// Get an instance of System.Globalization.RegionInfo object based on country name. 
        /// </summary>
        public RegionInfo RegionInfo
        {
            get
            {
                return new RegionInfo(ISO);
            }
        }
    }
}
