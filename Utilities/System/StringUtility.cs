﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlKhemeia.Utilities
{
   public class StringUtility
   {
      /// <summary>
      /// Generate a list of alphabets from A to Z
      /// </summary>
      /// <returns>Returns a list of string</returns>
      public static List<string> GetListOfAlphabets()
      {
         List<string> alphabets = new List<string>();

         for (char c = 'A'; c <= 'Z'; ++c)
         {
            alphabets.Add(c.ToString());
         }
         return alphabets;
      }
   }
}
