﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlKhemeia.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public struct Position
    {
        public double Latitude;
        public double Longitude;
    }

    /// <summary>
    /// 
    /// </summary>
    public class MathUtility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position1"></param>
        /// <param name="position2"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        public static double CalculateDistance(Position position1, Position position2, char unit = 'K')
        {
            return Distance(position1.Latitude, position1.Longitude, position2.Latitude, position2.Longitude, unit);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lat1"></param>
        /// <param name="lon1"></param>
        /// <param name="lat2"></param>
        /// <param name="lon2"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        private static double Distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = System.Math.Sin(deg2rad(lat1)) * System.Math.Sin(deg2rad(lat2)) + System.Math.Cos(deg2rad(lat1)) * System.Math.Cos(deg2rad(lat2)) * System.Math.Cos(deg2rad(theta));
            dist = System.Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        /// <summary>
        /// This function converts decimal degrees to radians
        /// </summary>
        /// <param name="deg"></param>
        /// <returns></returns>
        private static double deg2rad(double deg)
        {
            return (deg * System.Math.PI / 180.0);
        }

        /// <summary>
        /// This function converts radians to decimal degrees
        /// </summary>
        /// <param name="rad"></param>
        /// <returns></returns>
        private static double rad2deg(double rad)
        {
            return (rad / System.Math.PI * 180.0);
        }
    }
}
