﻿/// <reference path="../angular.js" />

var app = angular.module("geo", []);
app.constant("allCountriesUrl", "http://localhost:38687/api/Country");
app.controller("countryCtrl", function ($scope, $http) {
    $scope.data = {};
    $scope.allCountries = function () {
        var promise = $http.get(allCountriesUrl);
        promise.success(function (data) {
            $scope.data.countries = data;
        });
        promise.error(function (error) {
            $scope.data.error = error;
        });
        return $scope.data.countries;
    }
});