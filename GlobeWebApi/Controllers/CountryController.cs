﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AlKhemeia.Geo;

namespace GlobeWebApi.Controllers
{
    public class CountryController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Country> Get()
        {
            return Globe.Countries.FindAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iso"></param>
        /// <returns></returns>
        [Route("country/iso")]
        public Country Get(string iso)
        {
            return Globe.Countries.FindCountry(iso);
        }

        /// <summary>
        /// Documentation
        /// </summary>
        /// <param name="name">country name</param>
        /// <returns>Country5</returns>
        [Route("country/name")]
        public Country GetByName(string name)
        {
            return Globe.Countries.FindByName(name);
        }

        
    }
}
